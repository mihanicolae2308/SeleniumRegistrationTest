import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.Set;

/**
 * Created by Mihai on 27-Sep-17.
 */
public class CookieTest extends BaseTest {

    @Test
    public void cookieTest() {
        driver.navigate().to("http://a1-tausandbox.rhcloud.com/w/cookie.html");
//        driver.get("http://a1-tausandbox.rhcloud.com/w/cookie.html");

        driver.manage().deleteAllCookies();
        Cookie cookie = new Cookie("aVeryNiceCookie", "12345");
        driver.manage().addCookie(cookie);

        Set<Cookie> cookieList = driver.manage().getCookies();

        System.out.println("The existing cookie(s) before pressing 'Remove the cookie' button:");
        for (Cookie c : cookieList) {
            System.out.println("Name: " + c.getName() + ", value: " + c.getValue());
        }

        WebElement deleteCookie = driver.findElement(By.id("delete-cookie"));
        deleteCookie.click();

        mySleeper();

        Set<Cookie> cookieListAfterDelete = driver.manage().getCookies();

        System.out.println("The existing cookie(s) after pressing 'Remove the cookie' button:");
        for (Cookie c : cookieListAfterDelete) {
            System.out.println("Name: " + c.getName() + ", value: " + c.getValue());
        }

        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();

        mySleeper();

        Set<Cookie> cookieListAfterSet = driver.manage().getCookies();

        System.out.println("The existing cookie(s) after pressing 'Set the cookie' button:");
        for (Cookie c : cookieListAfterSet) {
            System.out.println("Name: " + c.getName() + ", value: " + c.getValue());
        }

        deleteCookie.click();

        mySleeper();

        Set<Cookie> cookieListAfterSecondDelete = driver.manage().getCookies();

        System.out.println("The existing cookie(s) after the second press of 'Remove the cookie' button:");
        for (Cookie c : cookieListAfterSecondDelete) {
            System.out.println("Name: " + c.getName() + ", value: " + c.getValue());
        }

    }

}
