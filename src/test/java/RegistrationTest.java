import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Mihai on 19-Sep-17.
 */
public class RegistrationTest extends BaseTest {

//Another way of defining a data provider//////////////
//    @DataProvider(name = "RegistrationDataProvider")
//    public static Object[][] registrationDataProvider() {
//        return new Object[][] { { "tyuio", "", "ccc", "" },
//                { "", "","", "" }};
//
//    }
    @DataProvider(name = "RegistrationDataProvider")
    public Iterator<Object[]> registrationDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"Mihai", ""});
        dp.add(new String[] {"T","Invalid input. Please enter between 2 and 35 letters"});
        dp.add(new String[] {"", "Invalid input. Please enter between 2 and 35 letters"});
        dp.add(new String[] {"Ty",""});
        dp.add(new String[] {"Alongerthanusualname",""});
        dp.add(new String[] {"Thisisaverylongnameweqweqrwqrqrqrqrwrwqr","Invalid input. Please enter between 2 and 35 letters"});
        return dp.iterator();
    }

    @DataProvider(name = "EmailDataProvider")
    public Iterator<Object[]> emailDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", "Invalid input. Please enter a valid email address"});
        dp.add(new String[] {"a","Invalid input. Please enter a valid email address"});
        dp.add(new String[] {"aklsdjas", "Invalid input. Please enter a valid email address"});
        dp.add(new String[] {"ada@","Invalid input. Please enter a valid email address"});
        dp.add(new String[] {"@tyuiop","Invalid input. Please enter a valid email address"});
        dp.add(new String[] {"a@b",""});
        dp.add(new String[] {"mihai@scoalainformala.com",""});
        return dp.iterator();
    }

    @DataProvider(name = "UsernameDataProvider")
    public Iterator<Object[]> usernameDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"", "Invalid input. Please enter between 4 and 35 letters or numbers"});
        dp.add(new String[] {"a","Invalid input. Please enter between 4 and 35 letters or numbers"});
        dp.add(new String[] {"ab", "Invalid input. Please enter between 4 and 35 letters or numbers"});
        dp.add(new String[] {"abc","Invalid input. Please enter between 4 and 35 letters or numbers"});
        dp.add(new String[] {"123","Invalid input. Please enter between 4 and 35 letters or numbers"});
        dp.add(new String[] {"abcd",""});
        dp.add(new String[] {"1234",""});
        dp.add(new String[] {"username123",""});
        dp.add(new String[] {"longerusername_123456789",""});
        return dp.iterator();
    }


    @Test(dataProvider = "RegistrationDataProvider")
    public void firstNameTest(String firstName, String firstNameError) {
        try {
            driver.navigate().to("http://a1-tausandbox.rhcloud.com/w/auth.html");

            RegistrationPage rp = PageFactory.initElements(driver, RegistrationPage.class);

            rp.firstNameInput(firstName);

            mySleeper();

            assert rp.checkFirstNameErrors(firstNameError);

        } catch (NullPointerException e) {
            System.out.println("No driver to be used!");
        }
    }

    @Test(dataProvider = "RegistrationDataProvider")
    public void lastNameTest(String lastName, String lastNameError) {
        try {
            driver.navigate().to("http://a1-tausandbox.rhcloud.com/w/auth.html");

            RegistrationPage rp = PageFactory.initElements(driver, RegistrationPage.class);

            rp.lastNameInput(lastName);

            mySleeper();

            assert rp.checkLastNameErrors(lastNameError);

        } catch (NullPointerException e) {
            System.out.println("No driver to be used!");
        }
    }

    @Test(dataProvider = "EmailDataProvider")
    public void emailTest(String email, String emailError) {
        try {
            driver.navigate().to("http://a1-tausandbox.rhcloud.com/w/auth.html");

            RegistrationPage rp = PageFactory.initElements(driver, RegistrationPage.class);

            rp.emailInput(email);

            mySleeper();

            assert rp.checkEmailErrors(emailError);

        } catch (NullPointerException e) {
            System.out.println("No driver to be used!");
        }
    }

    @Test(dataProvider = "UsernameDataProvider")
    public void usernameTest(String username, String usernameError) {
        try {
            driver.navigate().to("http://a1-tausandbox.rhcloud.com/w/auth.html");

            RegistrationPage rp = PageFactory.initElements(driver, RegistrationPage.class);

            rp.usernameInput(username);

            mySleeper();

            assert rp.checkUsernameErrors(usernameError);

        } catch (NullPointerException e) {
            System.out.println("No driver to be used!");
        }
    }
}
