import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * Created by Mihai on 19-Sep-17.
 */



public class WebBrowser {

    public enum Browsers {
        FIREFOX,
        IE,
        CHROME
    }

    public static WebDriver getDriver(Browsers browserName) {
        WebDriver driver = null;
        switch(browserName) {
            case IE:
                System.setProperty("webdriver.ie.driver", "src\\main\\resources\\drivers\\IEDriverServer.exe");
                return new InternetExplorerDriver();
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\drivers\\chromedriver.exe");
                return new ChromeDriver();
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\drivers\\geckodriver.exe");
                return new FirefoxDriver();
            default:
                throw new IllegalArgumentException("Invalid browser");

            }
    }
}
