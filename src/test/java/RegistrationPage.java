import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Mihai on 19-Sep-17.
 */
public class RegistrationPage {

    @FindBy(how = How.ID, using = "inputFirstName")
    private WebElement firstNameField;

    @FindBy(how = How.ID, using = "inputLastName")
    private WebElement lastNameField;

    @FindBy(how = How.ID, using = "register-submit")
    private WebElement submitField;

    @FindBy(how = How.ID, using = "registration_form")
    private WebElement registrationForm;

    @FindBy(how = How.ID, using = "inputEmail")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "inputUsername")
    private WebElement usernameField;

    @FindBy(how = How.CLASS_NAME, using = "list-unstyled")
    public List<WebElement> errors;

    public boolean checkFirstNameErrors(String firstNameError) {

        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        System.out.println("First name length is: " + firstNameField.getAttribute("value").length());
        if (firstNameField.getAttribute("value").length() >= 2 && firstNameField.getAttribute("value").length() <= 35) {
            System.out.println("There are " + errors.size() + " errors!");
            return errors.size() == 0;
        } else {
            System.out.println("The error is: " + errors.get(0).getText());
            return errors.get(0).getText().compareTo(firstNameError) == 0;
        }
    }

    public boolean checkLastNameErrors(String lastNameError) {

        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        System.out.println("Last name length is: " + lastNameField.getAttribute("value").length());
        if (lastNameField.getAttribute("value").length() >= 2 && lastNameField.getAttribute("value").length() <= 35) {
            System.out.println("There are " + errors.size() + " errors!");
            return errors.size() == 0;
        } else {
            System.out.println("The error is: " + errors.get(0).getText());
            return errors.get(0).getText().compareTo(lastNameError) == 0;
        }
    }

    public boolean checkEmailErrors(String emailError) {

        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        if (emailField.getAttribute("value").length() >=3 && emailField.getAttribute("value").contains("@") &&
                ! emailField.getAttribute("value").startsWith("@") && ! emailField.getAttribute("value").endsWith("@")) {
            System.out.println("There are " + errors.size() + " errors!");
            return errors.size() == 0;
        } else {
            System.out.println("The error is: " + errors.get(0).getText());
            return errors.get(0).getText().equals(emailError);
        }
    }

    public boolean checkUsernameErrors(String usernameError) {

        for (WebElement we : errors) {
            System.out.println("Class list-ustyled text: " + we.getText());
        }

        if (usernameField.getAttribute("value").length() >= 4 && usernameField.getAttribute("value").length() <= 35) {
            System.out.println("There are " + errors.size() + " errors!");
            return errors.size() == 0;
        } else {
            System.out.println("The error is: " + errors.get(0).getText());
            return errors.get(0).getText().equals(usernameError);
        }
    }

    public void firstNameInput(String fn) {
        firstNameField.clear();
        firstNameField.sendKeys(fn);
        System.out.println("Input text is: " +  firstNameField.getAttribute("value"));
        registrationForm.click();
        //submitField.submit();
    }

    public void lastNameInput(String ln) {
        lastNameField.clear();
        lastNameField.sendKeys(ln);
        System.out.println("Input text is: " +  lastNameField.getAttribute("value"));
        registrationForm.click();
    }

    public void emailInput(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        System.out.println("Email address that was introduced: " + emailField.getAttribute("value"));
        registrationForm.click();
    }

    public void usernameInput(String username) {
        usernameField.clear();
        usernameField.sendKeys(username);
        System.out.println("Email address that was introduced: " + usernameField.getAttribute("value"));
        registrationForm.click();
    }



}
