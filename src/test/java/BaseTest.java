import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by Mihai on 19-Sep-17.
 */
public class BaseTest {
    WebDriver driver;

    public void mySleeper() {
        try {
            Thread.sleep(1000);
        }
        catch (Exception ex) {

        }
    }


    @BeforeTest
    public void startDriver() {
        //WebDriver driver = new FirefoxDriver();
        try {
            //driver = WebBrowser.getDriver("FIREFOX");
            driver = WebBrowser.getDriver(WebBrowser.Browsers.FIREFOX);
        }
        catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

    }



    @AfterTest
    public void closeDriver() {
        if (driver != null) {
            driver.close();
        } else {
            System.out.println("No driver to be closed.");
        }

    }
}
